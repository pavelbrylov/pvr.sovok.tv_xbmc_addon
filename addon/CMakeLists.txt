configure_file(addon.xml.in addon.xml)

install(FILES icon.png ${CMAKE_CURRENT_BINARY_DIR}/addon.xml DESTINATION ${PROJECT_NAME})
install(DIRECTORY resources DESTINATION ${PROJECT_NAME})
