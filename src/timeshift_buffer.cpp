/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <libXBMC_addon.h>
#include "timeshift_buffer.h"

using namespace std;
using namespace ADDON;
using namespace PLATFORM;

#ifdef DeleteFile
#undef DeleteFile
#endif

TimeshiftBuffer::TimeshiftBuffer(CHelper_libXBMC_addon *addonHelper, const string &streamUrl, const string &bufferPath) :
    m_addonHelper(addonHelper),
    m_bufferPath(bufferPath)
{
    m_streamHandle = m_addonHelper->OpenFile(streamUrl.c_str(), 0);
    if (!m_streamHandle)
        throw InputBufferException();

    m_bufferWriteHandle = m_addonHelper->OpenFileForWrite(m_bufferPath.c_str(), true);
    if (!m_bufferWriteHandle)
    {
        m_addonHelper->CloseFile(m_streamHandle);
        throw InputBufferException();
    }

    m_bufferReadHandle = m_addonHelper->OpenFile(m_bufferPath.c_str(), 0);
    if (!m_bufferReadHandle)
    {
        m_addonHelper->CloseFile(m_streamHandle);
        m_addonHelper->CloseFile(m_bufferWriteHandle);
        m_addonHelper->DeleteFile(m_bufferPath.c_str());
        throw InputBufferException();
    }

    CreateThread();
}

TimeshiftBuffer::~TimeshiftBuffer()
{
    StopThread();

    m_addonHelper->CloseFile(m_streamHandle);
    m_addonHelper->CloseFile(m_bufferWriteHandle);
    m_addonHelper->CloseFile(m_bufferReadHandle);

    m_addonHelper->DeleteFile(m_bufferPath.c_str());
}

void *TimeshiftBuffer::Process()
{
    unsigned char buffer[8192];

    while (!IsStopped())
    {
        unsigned int bytesRead = m_addonHelper->ReadFile(m_streamHandle, buffer, sizeof(buffer));
        m_addonHelper->WriteFile(m_bufferWriteHandle, buffer, bytesRead);
    }

    return NULL;
}

long long TimeshiftBuffer::GetLength() const
{
    return m_addonHelper->GetFileLength(m_bufferReadHandle);
}

long long TimeshiftBuffer::GetPosition() const
{
    return m_addonHelper->GetFilePosition(m_bufferReadHandle);
}

int TimeshiftBuffer::Read(unsigned char *buffer, unsigned int bufferSize)
{
    CLockObject lock(m_mutex);

    unsigned int totalBytesRead = 0;
    unsigned int totalTimeWaiting = 0;

    while (totalBytesRead < bufferSize && !IsStopped())
    {
        unsigned int bytesRead = m_addonHelper->ReadFile(m_bufferReadHandle, buffer, bufferSize - totalBytesRead);
        totalBytesRead += bytesRead;
        buffer += bytesRead;

        if (totalBytesRead < bufferSize)
        {
            Sleep(50);
            totalTimeWaiting += 50;
        }
    }

    if (totalTimeWaiting >= 10000)
        StopThread();

    return totalBytesRead;
}

long long TimeshiftBuffer::Seek(long long iPosition, int iWhence) const
{
    return m_addonHelper->SeekFile(m_bufferReadHandle, iPosition, iWhence);
}

bool TimeshiftBuffer::SwitchStream(const string &newUrl)
{
    CLockObject lock(m_mutex);

    StopThread();
    m_addonHelper->SeekFile(m_bufferReadHandle, 0, SEEK_END);
    m_addonHelper->CloseFile(m_streamHandle);

    m_streamHandle = m_addonHelper->OpenFile(newUrl.c_str(), 0);
    CreateThread();

    return m_streamHandle != NULL;
}
